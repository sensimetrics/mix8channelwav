/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

let hardcodedPathURL = 'file:///storage/emulated/0/'

let hardcodedMaskerName = 'Make8Noise.wav'
// let hardcodedMaskerName = 'white_noise.wav'

let hardcodedSignalName = 'Make8Long.wav'


var app = {
    // Application Constructor
    initialize: function () {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function () {
        if (navigator.notification) { // Override default HTML alert with native dialog
            window.alert = function (message) {
                navigator.notification.alert(
                    message, // message
                    null, // callback
                    'Mix8ChannelWavDemo', // title
                    'OK' // buttonName
                )
            }
        }

        let listDir = function (path) {
            $("#audioFileList").empty();

            window.resolveLocalFileSystemURL(path,
                function (fileSystem) {
                    var reader = fileSystem.createReader();
                    reader.readEntries(
                        function (entries) {
                            console.log(entries);
                            entries.forEach(function (entry) {
                                var ext = entry.name.split(".").pop();
                                if (entry.isFile && (ext.toUpperCase() === "WAV")) {
                                    $("#audioFileList").append("<option value='" + entry.name + "'>" + entry.name + "</option>");
                                }
                            });
                        },
                        function (err) {
                            console.log(err);
                            alert(err);
                        }
                    );
                }, function (err) {
                    console.log(err);
                    alert(err);
                }
            );
        }

        // function performing mode-switch
        $('#chooseMode').on("change", function () {
            $('.mixMode').hide();

            $('.mixMode').slideUp();
            $("." + $(this).val()).slideDown();

            selectedFileList = [];
            listDir(hardcodedPathURL);
        });

        // function populating list of selected files in new mode
        $('#audioFileList').on("change", function (event) {
            buildChannelPickers();
        });

        // function starting mixing of selected channels into output
        $('#startProcessingSelected').on("click", function (event) {
            doStartProcessingSelected(8, combineSelected);
        });
        $('#resetSelected').click(function (evt) {
            doResetSelected();
        })

        const setStartProcessingButtonState = function (isEnabled) {
            if (isEnabled) {
                // enable UI
                $('#startProcessing').prop('disabled', false).css({ backgroundColor: 'blue' })
                $('#reset').prop('disabled', false).css({ backgroundColor: 'blue' })
            } else {
                // disable UI
                $('#startProcessing').prop('disabled', true).css({ backgroundColor: '#000000' })
                $('#reset').prop('disabled', true).css({ backgroundColor: '#000000' })
            }

            $('#startProcessing').click(function (evt) {
                doStartProcessing(8, combineSignalAndMasker);
            })
            $('#reset').click(function (evt) {
                doReset();
            })
        };

        // request read access to the external storage if we don't have it
        cordova.plugins.diagnostic.getExternalStorageAuthorizationStatus(function (status) {
            if (status === cordova.plugins.diagnostic.permissionStatus.GRANTED) {
                alert('External storage use is authorized')
                console.log('External storage use is authorized')
                setStartProcessingButtonState(true)
            } else {
                cordova.plugins.diagnostic.requestExternalStorageAuthorization(function (result) {
                    // alert('result: ' +  JSON.stringify(result, null, '\t'))
                    // this revealed that the plugin documentation is no longer correct
                    let authorized = result['READ_EXTERNAL_STORAGE'] === 'GRANTED'
                    alert('Authorization request for external storage use was ' + (authorized ? 'granted' : 'denied'))
                    console.log('Authorization request for external storage use was ' + (authorized ? 'granted' : 'denied'))
                    setStartProcessingButtonState(authorized)
                }, function (error) {
                    alert(error)
                    console.error(error)
                })
            }
        }, function (error) {
            alert('The following error occurred: ' + error)
            console.error('The following error occurred: ' + error)
            setStartProcessingButtonState(false)
        });
    }


};

app.initialize();


function buildChannelPickers() {
    let selectedFileChannelPickers = $("ul.selectedFiles li.filenameBorder");
    // alert("sfcp #: " + selectedFileChannelPickers.length)
    let selectedFileList = [];
    for (i = 0; i < selectedFileChannelPickers.length; i++) {
        let t = $("span", selectedFileChannelPickers[i]).text();
        // alert("i: " + i + " t: " + t);
        selectedFileList.push(t);
    }
    // alert("selectedFileList: " + JSON.stringify(selectedFileList));
    let selectList = $('#audioFileList').val();
    // alert("here: " + JSON.stringify(selectList));
    // add new selections if there are any
    if (selectList !== null) {
        selectList.forEach(function (filename) {
            // alert("selectedFileList.includes(filename): " + selectedFileList.includes(filename))
            if ((selectedFileList.length === 0) || !selectedFileList.includes(filename)) {
                // alert("about to add: " + filename);
                makeChannelPickBoxHTML(filename);
            }
        });
    }

    // remove unselected ones
    // re-init list on screen
    selectedFileChannelPickers = $("ul.selectedFiles li.filenameBorder");

    let removedPicker = false;
    for (i = 0; i < selectedFileChannelPickers.length; i++) {
        let t = $("span", selectedFileChannelPickers[i]).text();
        if (selectList === null || selectList.length === 0 || !selectList.includes(t)) {
            $(selectedFileChannelPickers[i]).remove();
            removedPicker = true;
        }
    }

    if (removedPicker) {
        channelSelectionChanged();
    }
}

function channelSelectionChanged() {
    // iterate over all the channel pickers, collecting valid entries
    let channelPickers = $("ul.selectedFiles li.filenameBorder");
    // test: each one should have data!
    // for (i = 0; i < channelPickers.length; i++) {
    // alert("i: " + i + " e: " + channelPickers[i])
    // let decoded = $(channelPickers[i]).data("decoded")
    // alert('decoded.sampleRate: ' + decoded.sampleRate);
    // alert('decoded.bitDepth: ' + decoded.bitDepth);
    // alert('decoded.channels dimensions: ' + '[' + decoded.channels.length + ',' + decoded.channels[0].length + ']');
    // alert('decoded.length: ' + decoded.length);
    // }

    // if there are valid entries, enable "start processing selected channels" button
    let outputChannelSelected = false;
    for (i = 0; i < channelPickers.length; i++) {
        // alert("cp[i]: " + JSON.stringify(channelPickers[i].outerHTML))
        let sels = $(".output-channel-select", channelPickers[i]);
        for (j = 0; j < sels.length; j++) {
            // alert("sels[j]: " + JSON.stringify(sels[j].outerHTML))
            if ($(sels[j]).val() !== "None") {
                outputChannelSelected = true;
                break;
            }
        }
    }

    if (outputChannelSelected) {
        $("#startProcessingSelected").prop('disabled', false).css({ backgroundColor: 'blue' })
    } else {
        $("#startProcessingSelected").prop('disabled', true).css({ backgroundColor: 'white' })
    }
}

// open the file, read it
function readResolveURL(path, resolve) {
    return new Promise(resolve => {
        var decoded = {}

        resolveURL(path).then(function (fe) {
            return readFileEntry(fe)
        }).then(function (data) {
            decoded = decodeWAV(data);

            // alert('decoded.sampleRate: ' + decoded.sampleRate);
            // alert('decoded.bitDepth: ' + decoded.bitDepth);
            // alert('decoded.channels dimensions: ' + '[' + decoded.channels.length + ',' + decoded.channels[0].length + ']');
            // alert('decoded.length: ' + decoded.length);

            resolve(decoded)
        })
    })
}

function makeChannelPickBoxHTML(filename) {
    // open the file, read it (temporarily), loop for # of channels.
    // alert("looking for " + hardcodedPathURL + filename);
    readResolveURL(hardcodedPathURL + filename).then(
        function (decoded) {
            // alert('decoded.sampleRate: ' + decoded.sampleRate);
            // alert('decoded.bitDepth: ' + decoded.bitDepth);
            // alert('decoded.channels dimensions: ' + '[' + decoded.channels.length + ',' + decoded.channels[0].length + ']');
            // alert('decoded.length: ' + decoded.length);

            let nc = decoded.channels.length;
            let selectors = "";
            for (i = 0; i < nc; i++) {
                let temp = make8ChannelSelectsHTML(filename, i)
                // alert("temp: " + temp)
                if (i === 0) {
                    selectors = temp;
                } else {
                    selectors = (selectors + temp)
                }
                // alert("selectors: " + selectors)
            }

            handmadeComponent = "<li class='filenameBorder'>" +
                "<span>" +
                filename +
                "</span>" +
                "<div>Channels:</div><br>" +
                // make8ChannelSelectHTML(filename, 0) +
                // make8ChannelSelectHTML(filename, 1) +
                selectors +
                "</li>";
            // alert("hc: " + handmadeComponent);

            // $('#selectedFiles').append(handmadeComponent).get(0).data("decoded", decoded);
            let hc = $(handmadeComponent);
            hc.data("decoded", decoded);
            hc.appendTo('#selectedFiles');

            $(".output-channel-select").on("change", channelSelectionChanged)
            $(".add-output-channel-select").on("click", function(event){
                // $(event.target).before("<label class='inline-block'>This is me!</label>");
                // alert("my parent: " + $(event.target).parent().get(0));
                // alert("count: " +  $(".output-channel-select", $(event.target).parent().get(0)).length)
                const parent = $(event.target).parent().get(0);
                const countExistingSelectors = $(".output-channel-select", parent).length;
                if (countExistingSelectors >= 8) {
                    // alread mapped to each output channel - no more!
                    return;
                }
                const currentChannel = $("label", parent).val();
                const filenameChannelId = "" + filename + "_" + currentChannel + "_" + countExistingSelectors;
                const filenameGainId = "" + filename + "_gain_" + currentChannel + "_" + countExistingSelectors;
                const newSelector = makeChannelSelectGainHTML(filenameChannelId, filenameGainId);
                $(event.target).before(newSelector);
            });
        })
}

function make8ChannelSelectsHTML(filename, inputChannel, outputCount = 0) {
    const filenameChannelId = "" + filename + "_" + inputChannel + "_" + outputCount;
    const filenameGainId = "" + filename + "_gain_" + inputChannel + "_" + outputCount;
    return "<div class='input-channel-maps'>" +
        "<label for='" + filenameChannelId + "'>" + "" + inputChannel + "</label>" +
        makeChannelSelectGainHTML(filenameChannelId, filenameGainId) +
        // "<select class='inline-block output-channel-select' id='" + filenameChannelId + "'>" +
        // "<option value='None'>None</option>" +
        // "<option value='0'>0</option>" +
        // "<option value='1'>1</option>" +
        // "<option value='2'>2</option>" +
        // "<option value='3'>3</option>" +
        // "<option value='4'>4</option>" +
        // "<option value='5'>5</option>" +
        // "<option value='6'>6</option>" +
        // "<option value='7'>7</option>" +
        // "</select>" +
        // "<label class='inline-block' for='" + filenameGainId + "'>Gain:</label>" +
        // "<input type='number' class='inline-block output-channel-gain' id='" + filenameGainId + "' min='0.0' max='1.0' value='1.0' placeholder='Enter gain'>" +
        "<button class='inline-block add-output-channel-select'>+</button>" +
        "</div>";
}

function makeChannelSelectGainHTML(filenameChannelId, filenameGainId) {
    return "<span>" +
    "<select class='inline-block output-channel-select' id='" + filenameChannelId + "'>" +
    "<option value='None'>None</option>" +
    "<option value='0'>0</option>" +
    "<option value='1'>1</option>" +
    "<option value='2'>2</option>" +
    "<option value='3'>3</option>" +
    "<option value='4'>4</option>" +
    "<option value='5'>5</option>" +
    "<option value='6'>6</option>" +
    "<option value='7'>7</option>" +
    "</select>" +
    "<label class='inline-block' for='" + filenameGainId + "'>Gain:</label>" +
    "<input type='number' class='inline-block output-channel-gain' id='" + filenameGainId + "' min='0.0' max='1.0' value='1.0' placeholder='Enter gain'>" +
    "</span>";
}

async function doReset() {
    $('#loadSignal').prop('checked', false)
    $('#loadMasker').prop('checked', false)
    $('#addSignalMasker').prop('checked', false)
    $('#writeCombined').prop('checked', false)
    $('#playCombined').prop('checked', false)
}

async function doResetSelected() {
    $('#loadSelectedFiles').prop('checked', false)
    $('#mixSelectedFiles').prop('checked', false)
    $('#writeMixedFile').prop('checked', false)
    $('#playMixedFile').prop('checked', false)

    $('#resetSelected').prop('disabled', true).css({ backgroundColor: '#000000' })
}


function doStartProcessingSelected(channelsOut, combinerFunction) {
    let startProcessing, startMixing, endMixing, startWriting, endProcessing;
    startProcessing = new Date();

    let channelPickers = $("ul.selectedFiles li.filenameBorder");
    // alert("channelPickers.length: " + channelPickers.length)
    let dcs = [];
    let channelMaps = [];

    // each one should have data!
    for (i = 0; i < channelPickers.length; i++) {
        // alert("here - i: " + i + " e: " + channelPickers[i])
        let decoded = $(channelPickers[i]).data("decoded")
        // alert('decoded.length: ' + decoded.length);
        dcs[i] = decoded;

        let channels = $("div.input-channel-maps", channelPickers[i]);
        // alert("channels.length: " + channels.length);
        let channelOutputs = [];
        for (k = 0; k < channels.length; k++) {
            let channelMap = [];
            let sels = $(".output-channel-select", channels[k]);
            for (j = 0; j < sels.length; j++) {
                let sel = $(sels[j]);
                let channelSelectorValue = sel.val();
                let channelGain = $(".output-channel-gain", sel.parent()).val();
                // alert("channelSelectorValue: " + channelSelectorValue)
                // alert("channelGain: " + channelGain)
                channelMap[j] = { "outputChannel": channelSelectorValue, "outputGain": channelGain };
                // alert("channelMap[" + j + "]: " + JSON.stringify(channelMap[j]))
            }
            channelOutputs[k] = channelMap;
        }
        channelMaps[i] = channelOutputs;
    }
    // alert("dcs.length: " + dcs.length)
    alert("channelMaps: " + JSON.stringify(channelMaps))

    $("#loadSelectedFiles").prop('checked', true)

    startMixing = new Date();
    mixed = combinerFunction(channelsOut, dcs, channelMaps)
    // alert("mixed.length: " + mixed.length)
    endMixing = new Date();

    $("#mixSelectedFiles").prop('checked', true)

    let promiseWrite = new Promise(function (resolve, reject) {
        let outputName = 'tmp' + makeRandomName(10) + '.wav';
        startWriting = new Date();
        resolve(writeWAV(outputName, mixed /*data*/));
    });

    promiseWrite.then(function (name) {
        $('#writeMixedFile').prop('checked', true)
        endProcessing = new Date();

        let tmpPath = hardcodedPathURL + name

        return playTmpAudio(tmpPath)
    }).then(function (o) {
        const timeDiff = endProcessing - startProcessing; //in ms
        const mixingDiff = endMixing - startMixing;
        const writingDiff = endProcessing - startWriting;
        alert(
            'Audio duration: ' + o.duration + '\n' +
            'Mixing duration: ' + mixingDiff + '\n' +
            'Writing duration: ' + writingDiff + '\n' +
            'Total processing took ' + timeDiff + ' ms.'
        );
        $('#playMixedFile').prop('checked', true)

        // deleteTmpAudio(o.url);

        $('#resetSelected').prop('disabled', false).css({ backgroundColor: 'blue' })
    }).catch(function (err) {
        alert('caught err: ' + err)
    })
}

async function doStartProcessing(channelsOut, combinerFunction) {
    const wait = ms => new Promise(resolve => setTimeout(resolve, ms))
    // wait(500).then(() => $('#loadSignal').prop('checked', true))
    // wait(1000).then(() => $('#loadMasker').prop('checked', true))
    // wait(1500).then(() => $('#addSignalMasker').prop('checked', true))
    // wait(2000).then(() => $('#writeCombined').prop('checked', true))
    // wait(2500).then(() => $('#playCombined').prop('checked', true))

    if (channelsOut !== 8) {
        alert('Don\'t know how to handle channelsOut === ' + channelsOut)
        return
    }

    let signalPath = hardcodedPathURL + hardcodedSignalName
    let maskerPath = hardcodedPathURL + hardcodedMaskerName

    let decodedSignal = {}
    let decodedMasker = {}

    let startProcessing, endProcessing;

    startProcessing = new Date();
    Promise.all([
        readResolveURLCheckFlag(signalPath, '#loadSignal').then(function (dc) {
            decodedSignal = dc
            return dc
        }),
        readResolveURLCheckFlag(maskerPath, '#loadMasker').then(function (dc) {
            decodedMasker = dc
            return dc
        })
    ])
        .then(function (values) {
            // return combineSignalAndMasker({
            //     "channelsOut": channelsOut,
            //     "values": values
            // })
            return combinerFunction({
                "channelsOut": channelsOut,
                "values": values
            })
        })
        .then(function (summed) {
            $('#addSignalMasker').prop('checked', true)

            let outputName = 'tmp' + makeRandomName(10) + '.wav'

            return writeWAV(outputName, summed /*data*/)
        })
        .then(function (name) {
            $('#writeCombined').prop('checked', true)
            endProcessing = new Date();

            let tmpPath = hardcodedPathURL + name

            return playTmpAudio(tmpPath)
        })
        .then(function (o) {
            const timeDiff = endProcessing - startProcessing; //in ms
            alert('Duration was: ' + o.duration + ' Processing took ' + timeDiff + ' ms.')
            $('#playCombined').prop('checked', true)

            deleteTmpAudio(o.url);
        })
        .catch(function (err) {
            alert('caught err: ' + err)
        })
}

// async play functions
function deleteTmpAudio(url, resolve, reject) {
    return new Promise(resolve => {
        window.resolveLocalFileSystemURL(url, function (fe) {
            // dir.getFile(fileName, {create:false}, function(fileEntry) {
            fe.remove(resolve, reject)
            // })
        })
    })
}

function playTmpAudio(url, resolve, reject) {
    return new Promise(resolve => {
        var media = new Media(url, function () {
            let duration = media.getDuration()
            media.release()

            resolve({ 'duration': duration, 'url': url })
        }, function (err) {
            reject(err)
        })
        media.play()
    })
}

// async write functions
function makeRandomName(n) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < n; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function writeWAV(fileName, data, resolve, reject) {
    return new Promise(resolve => {
        let blob = encodeWAV(48000, data)

        resolveURLAndWrite(hardcodedPathURL, fileName, blob, function (result) {
            resolve(fileName)
        }, function (err) {
            alert('rURLAW failed: ' + err)
            reject(err)
        })
    })
}

async function resolveURLAndWrite(dirURL, fileName, fileBlob, outerResolve, outerFail) {
    return new Promise(resolve => {
        window.resolveLocalFileSystemURL(dirURL, function (dirEntry) {
            dirEntry.getFile(fileName, { create: true }, function (file) {
                file.createWriter(function (fileWriter) {
                    fileWriter.onwriteend = function (event) {
                        outerResolve(event)
                    }
                    fileWriter.write(fileBlob)
                })
            })
        }, function (err) {
            alert('err: ' + err)
            outerFail(err)
        });
    });
}

// async read functions
function readResolveURLCheckFlag(path, flag, resolve) {
    return new Promise(resolve => {
        var decoded = {}

        resolveURL(path).then(function (fe) {
            return readFileEntry(fe)
        })
            .then(function (data) {
                decoded = decodeWAV(data);

                // alert('decoded.sampleRate: ' + decoded.sampleRate);
                // alert('decoded.bitDepth: ' + decoded.bitDepth);
                // alert('decoded.channels dimensions: ' + '[' + decoded.channels.length + ',' + decoded.channels[0].length + ']');
                // alert('decoded.length: ' + decoded.length);

                $(flag).prop('checked', true)
                resolve(decoded)
            })
    })
}

function resolveURL(url, resolve) {
    return new Promise(resolve => {
        window.resolveLocalFileSystemURL(url, function (fe) {
            resolve(fe)
        }, function (err) {
            alert('err')
        })
    })
}

function readFileEntry(fe, resolve) {
    return new Promise(resolve => {
        let reader = new FileReader()
        reader.onloadend = function () {
            resolve(this.result)
        }

        fe.file(function (file) {
            reader.readAsArrayBuffer(file)
        })
    })
}

function combineSignalAndMasker(args, resolve) {
    const channelsOut = args.channelsOut;
    const values = args.values;
    const maxLength = Object.values(values) //get all the values of the object
        .reduce(function (largest, dc) {          //reduce the max for all dc
            return (largest > dc.channels[0].length ? largest : dc.channels[0].length)
        }, 0);

    return new Promise(resolve => {
        var mixed = null

        // this ONLY mixes 8-channel WAV files

        if (channelsOut === 8) {
            values.forEach(function (dc, audioIdx) {
                let nChan = dc.channels.length
                if (nChan !== 8) {
                    // TODO: return error
                    alert('invalid value of nChan ' + nChan);
                    reject('invalid value of nChan: ' + nChan);
                }
            });

            // mix
            mixed = [
                new Float32Array(maxLength),
                new Float32Array(maxLength),
                new Float32Array(maxLength),
                new Float32Array(maxLength),
                new Float32Array(maxLength),
                new Float32Array(maxLength),
                new Float32Array(maxLength),
                new Float32Array(maxLength)
            ]


            values.forEach(function (dc, audioIdx) {
                mixed = add8ToMix(mixed, dc.channels)
            });

            resolve(mixed);
        }
    })
}

function combineSelected(channelsOut, dcs, channelMaps) {
    // alert("dcs.length: " + dcs.length)
    // alert("channelMaps: " + JSON.stringify(channelMaps))
    const maxLength = dcs.reduce(function (largest, dc) {          //reduce the max for all dc
        return (largest > dc.channels[0].length ? largest : dc.channels[0].length)
    }, 0);

    // this only knows how to produce 9-channel outputs
    if (channelsOut != 8) {
        alert("can only handle 8 output channes, not " + channelsOut)
        return null;
    }

    // and # of decoded files  must match # of maps
    if (dcs.length !== channelMaps.length) {
        alert("internal error - number of files != number of channel maps")
        return null;
    }

    // mix
    let mixed = [
        new Float32Array(maxLength),
        new Float32Array(maxLength),
        new Float32Array(maxLength),
        new Float32Array(maxLength),
        new Float32Array(maxLength),
        new Float32Array(maxLength),
        new Float32Array(maxLength),
        new Float32Array(maxLength)
    ];

    // alert("dcs.length: " + dcs.length)
    dcs.forEach(function (dc, dcIndex) {
        let channelMap = channelMaps[dcIndex];
        channelMap.forEach(function (outputInfo, inputChannel) {
            if (Array.isArray(outputInfo)) {
                // TODO: loop over each element of outputInfo
                outputInfo.forEach(function (oi) {
                    // alert('oi: ' + JSON.stringify(oi));
                    mixChannelData(oi, dc, inputChannel, mixed);
                });
            } else {
                // alert('outputInfo: ' + JSON.stringify(outputInfo));
                mixChannelData(outputInfo, dc, inputChannel, mixed);
            }
        });
    });

    return mixed;
}

function mixChannelData(outputInfo, dc, inputChannel, mixed) {
    // get rid of ||-, &&-expressions too complicated to read
    if (outputInfo === null) {
        return;
    }

    const outputChannel = outputInfo["outputChannel"];
    if (outputChannel === "None") {
        // no output channel selected
        return;
    }
    if (isNaN(outputChannel)) {
        // some other error
        return;
    }
    if ((outputChannel < 0) || (outputChannel > 7)) {
        // not a valid output channel for 8-channel file
        return;
    }

    const channelData = dc.channels[inputChannel];
    addToMix(mixed, +outputChannel, channelData, +outputInfo["outputGain"]);
}

function addToMix(mixed, outputChannel, channelData, channelGain = 1.0) {

    channelData.forEach(function (sample, index) {
        mixed[outputChannel][index] += (sample * channelGain);
    })
}


function add8ToMix(mix, channels) {
    if ((mix.length !== 8) || (channels.length !== 8)) {
        alert('add2ToMix - mix.length or channels.length invalid!\n' +
            'Both must === 8\n' +
            'mix.length: ' + mix.length + '\n' +
            'channels.length: ' + channels.length
        )

        return null
    }

    let mixLongerEqual = mix[0].length >= channels[0].length
    let offset = Math.round(Math.abs(mix[0].length - channels[0].length) / 2.0)

    if (mixLongerEqual) {
        channels.forEach(function (channel, iC) {
            channel.forEach(function (v, iV) {
                mix[iC][iV + offset] += v;
            })
        })
        return mix
    } else {
        mix.forEach(function (m, iM) {
            m.forEach(function (v, iV) {
                channels[iM][iV + offset] += m;
            })
        })
        return channels
    }
}



// based on https://developers.google.com/web/updates/2012/06/How-to-convert-ArrayBuffer-to-and-from-String

function ab2str(buf) {
    return String.fromCharCode.apply(null, new Uint8Array(buf))
}

// based on https://github.com/oampo/audiofile.js/blob/master/audiofile.js
// wonderful code

function readString(data, offset, length) {
    return ab2str(data.slice(offset, offset + length))
};

function readIntL(data, offset, length) {
    var buf = new Uint8Array(data)
    var value = 0

    for (var i = 0; i < length; i++) {
        // value = value + ((data.charCodeAt(offset + i) & 0xFF) *
        //                 Math.pow(2, 8 * i));
        value = value + (buf[offset + i] * Math.pow(2, 8 * i))
    }
    return value
};

function readChunkHeaderL(data, offset) {
    var chunk = {}
    chunk.name = readString(data, offset, 4)
    chunk.length = readIntL(data, offset + 4, 4)
    return chunk
};

function decodeWAV(data) {
    var decoded = {}
    var offset = 0
    // Header
    var chunk = readChunkHeaderL(data, offset)
    offset += 8
    if (chunk.name != 'RIFF') {
        alert('File is not a WAV')
        return null
    }

    var fileLength = chunk.length
    fileLength += 8

    var wave = readString(data, offset, 4)
    offset += 4
    if (wave != 'WAVE') {
        alert('File is not a WAV')
        return null
    }

    while (offset < fileLength) {
        var chunk = readChunkHeaderL(data, offset)
        offset += 8
        if (chunk.name == 'fmt ') {
            // File encoding
            var encoding = readIntL(data, offset, 2)
            offset += 2

            if (encoding != 0x0001) {
                // Only support PCM
                alert('Cannot decode non-PCM encoded WAV file')
                return null
            }

            // Number of channels
            var numberOfChannels = readIntL(data, offset, 2)
            offset += 2

            // Sample rate
            var sampleRate = readIntL(data, offset, 4)
            offset += 4

            // Ignore bytes/sec - 4 bytes
            offset += 4

            // Ignore block align - 2 bytes
            offset += 2

            // Bit depth
            var bitDepth = readIntL(data, offset, 2)
            var bytesPerSample = bitDepth / 8
            offset += 2
        } else if (chunk.name == 'data') {
            // Data must come after fmt, so we are okay to use it's variables
            // here
            var length = chunk.length / (bytesPerSample * numberOfChannels)
            var channels = []
            for (var i = 0; i < numberOfChannels; i++) {
                channels.push(new Float32Array(length))
            }

            for (var i = 0; i < numberOfChannels; i++) {
                var channel = channels[i]
                for (var j = 0; j < length; j++) {
                    var index = offset
                    index += (j * numberOfChannels + i) * bytesPerSample
                    // Sample
                    var value = readIntL(data, index, bytesPerSample)
                    // Scale range from 0 to 2**bitDepth -> -2**(bitDepth-1) to
                    // 2**(bitDepth-1)
                    var range = 1 << bitDepth - 1
                    if (value >= range) {
                        value |= ~(range - 1)
                    }
                    // Scale range to -1 to 1
                    channel[j] = value / range
                }
            }
            offset += chunk.length
        } else {
            offset += chunk.length
        }
    }
    decoded.sampleRate = sampleRate
    decoded.bitDepth = bitDepth
    decoded.channels = channels
    decoded.length = length
    return decoded
};

// based on https://github.com/higuma/wav-audio-encoder-js/blob/master/lib/WavAudioEncoder.js
// and https://stackoverflow.com/questions/13814621/how-can-i-get-the-dimensions-of-a-multidimensional-javascript-array
function array_equals(a, b) {
    return a.length === b.length && a.every(function (value, index) {
        return value === b[index];
    })
}

function getdim(arr) {
    if (/*!(arr instanceof Array) || */!arr.length) {
        return []; // current array has no dimension
    }
    var dim = arr.reduce(function (result, current) {
        // check each element of arr against the first element
        // to make sure it has the same dimensions
        return array_equals(result, getdim(current)) ? result : false;
    }, getdim(arr[0]));

    // dim is either false or an array
    return dim && [arr.length].concat(dim);
}

function setString(view, offset, str) {
    let len = str.length
    for (var i = 0; i < len; ++i) {
        view.setUint8(offset + i, str.charCodeAt(i))
    }
}

// convert data all at once - no collecting chunks
function encodeWAV(sampleRate, data) {
    // expect max 2-D data
    let dims = getdim(data);
    if ((dims === []) || (dims.length <= 0) || (dims.length > 2) || (dims === false)) {
        alert('invalid dimensions in data')
        return null
    }

    // if 2-D, number of samples same
    let numChannels = data.length
    let numSamples = (numChannels == 1) ? dims[0] : dims[1];
    // alert("numChannels: " + numChannels + "\n" + "numSamples: " + numSamples)
    let dataView = new DataView(new ArrayBuffer(numSamples * numChannels * 2))
    var offset = 0

    for (var i = 0; i < numSamples; ++i) {
        for (var ch = 0; ch < numChannels; ++ch) {
            // we presume Float32Array - scale to 16-bit integer
            var sample = (numChannels === 1) ? data[i] : data[ch][i]
            sample *= 0x7fff
            sample = (sample < 0) ? Math.max(sample, -0x8000) : Math.min(sample, 0x7fff)
            dataView.setInt16(offset, sample, true)
            offset += 2
        }
    }

    // construct header ...

    let dataSize = numChannels * (numSamples * 2)
    let headerView = new DataView(new ArrayBuffer(44))

    setString(headerView, 0, 'RIFF')
    headerView.setUint32(4, 36 + dataSize, true)
    setString(headerView, 8, 'WAVE')
    setString(headerView, 12, 'fmt ')
    headerView.setUint32(16, 16, true)
    headerView.setUint16(20, 1, true)
    headerView.setUint16(22, numChannels, true)
    headerView.setUint32(24, sampleRate, true)
    headerView.setUint32(28, sampleRate * 4, true)
    headerView.setUint16(32, numChannels * 2, true)
    headerView.setUint16(34, 16, true)
    setString(headerView, 36, 'data')
    headerView.setUint32(40, dataSize, true)

    // put samples & data out
    let dataViews = []
    dataViews.push(dataView)
    dataViews.unshift(headerView)

    let blob = new Blob(dataViews, { type: 'audio/wav' })
    return blob
}
